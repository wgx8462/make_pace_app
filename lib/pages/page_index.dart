import 'package:flutter/material.dart';
import 'package:make_pace_app/pages/page_make_face.dart';

class PageIndex extends StatefulWidget {
  const PageIndex({super.key});

  @override
  State<PageIndex> createState() => _PageIndexState();
}

class _PageIndexState extends State<PageIndex> {

  String _friendName = '';


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blueGrey,
        centerTitle: true,
        title: const Text(
          '친구 얼굴 그리기',
          style: TextStyle(
            fontWeight: FontWeight.bold,
            color: Colors.white,
          ),
        ),
      ),
      body: Center(
        child: Container(
          child: Column(
            children: [
              TextField(
                onChanged: (text) {
                  setState(() {
                    _friendName = text;
                  });
                },
                decoration: InputDecoration(labelText: '친구이름'),
              ),
              OutlinedButton(
                onPressed: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => PageMakeFace(
                              friendName: _friendName,
                            )),
                  );
                },
                child: const Text('시작'),
              ),

            ],
          ),
        ),
      ),
    );
  }
}
