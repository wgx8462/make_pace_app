import 'package:flutter/material.dart';
import 'package:make_pace_app/pages/page_index.dart';

class PageMakeFace extends StatefulWidget {
  const PageMakeFace({super.key, required this.friendName});

  final String friendName;

  @override
  State<PageMakeFace> createState() => _PageMakeFaceState();
}

class _PageMakeFaceState extends State<PageMakeFace> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text('결과'),
        ),
        body: buildBody(context));
  }

  Widget buildBody(BuildContext context) {
    return SingleChildScrollView(
      child: Column(
        children: [
          Center(
            child: Stack(
              children: [
                Image.asset('assets/mouse_1.png'),
                Image.asset('assets/face_1.png'),
                Image.asset('assets/nose_1.png'),
                Image.asset('assets/eyes_1.png'),
                Image.asset('assets/eyebrow_1.png'),
              ],
            ),
          ),
          Center(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                const Text(
                  '친구 이름 : ',
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                Text(
                  widget.friendName,
                  style: TextStyle(
                    fontSize: 25,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          OutlinedButton(
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => PageIndex()));
              },
              child: Text(
                '종료',
              )),
        ],
      ),
    );
  }
}
